
public class ParserException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ParserException() {
		super("Parser errors");
	}
	
	public ParserException(String m) {
		super(m);
	}

}