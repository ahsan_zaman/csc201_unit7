/**
 * @author Ahsan Zaman
 * 
 * Algorithm:
 * 1. Declare and initialize wheels and weight to take user input.
 * 2. Take user double input.
 * 3. If user input is less then or equal to 0, throw Parser exception.
 * 4. Pass values into aCycle object.
 * 5. Display aCycle values.
 * 6. Write ACycle attributes to file.
 * 7. Read from file and display to user.
 */
import javax.swing.*;

public class CycleDemo {
	
	/**
	 * Main method for the whole program. Declares a Cycle object aCycle and two double variables wheels and weight.
	 * Takes input for wheels and weight and passes them into aCycle through overloaded constructor. 
	 * @param args
	 * @throws ParserException Thrown when there is an input mismatch.
	 * @throws InputLessThanZeroException Thrown when the input is less than or equal to 0.
	 */
	public static void main( String[] args ) throws ParserException, InputLessThanZeroException{
		Cycle aCycle = null;
		double wheels=0.0;
		double weight=0.0;
		try{
			wheels = Double.parseDouble( JOptionPane.showInputDialog( "Enter the number of wheels on the cycle: " ) );
			weight = Double.parseDouble( JOptionPane.showInputDialog( "Enter the weight of the cycle: " ) );
		} catch( Exception e ){
			JOptionPane.showMessageDialog( null, "Input Mismatch. Parser Exception thrown. " );
		}
		
		aCycle = new Cycle( wheels, weight );
		JOptionPane.showMessageDialog( null, aCycle.toString() );
		
		aCycle.WriteToFile( JOptionPane.showInputDialog( "Enter the file name to write Cycle data to: " )+".txt" );
		aCycle.ReadFromFile( JOptionPane.showInputDialog( "Enter the file name to read from: " )+".txt" );
	}
}
