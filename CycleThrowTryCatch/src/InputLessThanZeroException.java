
public class InputLessThanZeroException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Default constructor for when the exception is thrown for input is less than or equal to zero.
	 */
	public InputLessThanZeroException(){
		super( "Input is less than or equal to zero." );
	}
	
	/**
	 * Thrown when the input is less than or equal to zero. 
	 * @param m Given by user as exception description
	 */
	public InputLessThanZeroException( String m ){
		super( m );
	}
	
}
