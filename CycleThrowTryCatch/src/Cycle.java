import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

import javax.swing.JOptionPane;
public class Cycle extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private double numOfWheels;
	private double weight;
	
	/**
	 * This is Cycle's overloaded constructor. It initializes the attributes, numOfWheels and weight
	 * by values given by user. If the values are less than or equal to 0, then an exception is thrown.
	 * Otherwise the attributes are simply initialized.
	 * @param numOfWheels Number of wheels of the cycle
	 * @param weight The weight of the cycle
	 * @throws InputLessThanZeroException Thrown when weight or numOfWheels is less than or equal to 0
	 */
	public Cycle( double numOfWheels, double weight ) throws InputLessThanZeroException{
		if( numOfWheels<=0 || weight<=0 )
			throw new InputLessThanZeroException( "Values cannot be less than or equal to zero." );
		else{
			this.numOfWheels = numOfWheels;
			this.weight = weight;
		}
	}
	
	/**
	 * Simply returns a string with the attributes of class.
	 * @return Returns the attributes of the class in a string.
	 */
	public String toString(){
		return "Number of wheels: "+numOfWheels+"\n Weight: "+weight;
	}
	
	/**
	 * Creates a PrintWriter object to create a file by the name of fileName and writes the attributes of the class to it.
	 * FileNotFoundException is thrown if the method fails to create the file.
	 * @param fileName Name of the file to write the attributes of the class to. This will be given by the user.
	 */
	public void WriteToFile( String fileName ){
		File file = new File( fileName );
		PrintWriter output = null;
		if( !file.exists() ){
			try {
				output = new PrintWriter( fileName );
				JOptionPane.showMessageDialog( null, "File has been created." );
				output.println( numOfWheels );
				output.println( weight );
				output.close();
			} catch ( FileNotFoundException e ){
				JOptionPane.showMessageDialog( null, "There was a problem while creating the desired file. \nPlease check permission." );
			}
		}
		else {
			try{
				output = new PrintWriter( fileName );
				output.append( numOfWheels+"\n" );
				output.append( weight+"\n" );
				output.close();
			} catch( Exception e ){
				JOptionPane.showMessageDialog( null, "Problem while writing to file. " );
			}
		}
	}
	
	/**
	 * Checks if the filename given exists. If not, then the file is created with the attributes of the class written to it.
	 * If the file exists, it simply opens it up using inputScanner and stores the contents in the class attributes and 
	 * also displays it to the user.
	 * @param fileName Name of the file to read from. This will be given by the user.
	 */
	public void ReadFromFile( String fileName ){
		File file = new File( fileName );
		PrintWriter output = null;
		Scanner inputScanner = null;
		
		if( !file.exists() ){
			try {
				// Creating and writing to file in the event that it doesnt exist.
				output = new PrintWriter( fileName );
				JOptionPane.showMessageDialog( null, "File has been created." );
				output.println( numOfWheels );
				output.println( weight );
				output.close();
			} catch ( FileNotFoundException e ){
				JOptionPane.showMessageDialog( null, "There was a problem while creating the desired file. \nPlease check permission." );
			}
		}
		else {
			try {
				// Reading from file if it exists.
				inputScanner = new Scanner( file );
				numOfWheels = Double.parseDouble( inputScanner.nextLine() );
				weight = Double.parseDouble( inputScanner.nextLine() );
				JOptionPane.showMessageDialog( null, toString() );
				
			} catch( FileNotFoundException e ){
				JOptionPane.showMessageDialog( null, "Could not access the desired file." );
			}
		}
	}	
}
